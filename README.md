# Todos API

<hr>

### Requirements

- node: 16
- postgresql: latest || 12
- npm: 6.14.15

### How To Install

#### Manual (NPM/YARN/PNPM)

1. Clone this repo
2. Create `.env` file and write variables value

```bash
cp .env.example .env
```

3. Install all of the dependencies, you can use your favorite package manager, for example <b>pnpm</b>

```bash
pnpm i
```

4. Run the production server

```bash
pnpm start:prod
```

or, if you want to start as development server

```bash
pnpm start:dev
```

5. application is running on port 8080 by default, but if you want to change the built-in port, just change the `APP_PORT` value in .env file with another port number.

#### Docker Compose

1. Make sure that docker and docker-compose was already installed to your machine
2. run this command to starting up this app

```bash
docker-compose up -d
```

app is running on port 8080 and postgres is running on port

### Folder Structure

The folder structure of this project is inspired by clean code architecture that has layers of application and bussines processes.
I customize this folder structure just for fun, and experimental purpose. As we can see, there is 3 main folders in src directory.

Application folder contains application/framework adapters to forwarding responses and receive all incoming requests, validate requests, applying guard to endpoints, and config file.

Business folder contains core business logic and data transfer object. Object from application layer can communicate with entity only from services, and an endpoint request body must be statically type as the DTO for that endpoint, and the DTO is contains all of the validation rules.

Resources folder contains utils files.
