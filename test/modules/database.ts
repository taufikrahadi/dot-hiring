import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskChecklist } from '../../src/business/task-checklist/task-checklist.entity';
import { Task } from '../../src/business/task/task.entity';
import { User } from '../../src/business/user/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DB_HOST'),
        port: configService.get('DB_PORT'),
        username: configService.get('DB_USER'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME') + '-test',
        entities: [User, Task, TaskChecklist],
        synchronize: true,
        // cache: {
        //   alwaysEnabled: true,
        //   duration: 60,
        // },
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
