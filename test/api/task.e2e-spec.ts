import {
  HttpStatus,
  INestApplication,
  UnprocessableEntityException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TaskModule } from '../../src/application/api/task/task.module';
import { MainModule } from '../modules/main';
import { truncate } from '../utils/truncate';
import { TaskFactory } from '../factories/task.factory';
import { UserFactory } from '../factories/user.factory';
import { login } from '../utils/login';
import * as supertest from 'supertest';
import * as faker from 'faker';

describe('task e2e', () => {
  let app: INestApplication;
  const taskFactory = new TaskFactory();
  const userFactory = new UserFactory();

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [MainModule, TaskModule],
      providers: [],
    }).compile();

    app = module.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: (errors: ValidationError[]) => {
          const message = errors.map((error) => {
            const { property, constraints } = error;
            const keys = Object.keys(constraints);

            const msgs: string[] = [];

            keys.forEach((key) => {
              msgs.push(`${constraints[key]}`);
            });

            return {
              property,
              errors: msgs,
            };
          });

          throw new UnprocessableEntityException(message);
        },
      }),
    );

    await app.init();
  });

  beforeEach(async () => {
    await truncate();
  });

  describe('/task POST', () => {
    it('should return created task', async () => {
      const user = await userFactory.create();
      const token = login(user);

      const response = await supertest(app.getHttpServer())
        .post('/task')
        .send({
          title: 'judul',
          description: 'deskripsi',
          checklists: [{ title: 'judul checklist nya' }],
        })
        .set('Authorization', `Bearer ${token}`)
        .expect(HttpStatus.CREATED);

      expect(response.body).toMatchObject({
        id: expect.any(String),
        title: 'judul',
        description: 'deskripsi',
        userId: user.id,
        checklists: [
          {
            id: expect.any(String),
            title: 'judul checklist nya',
            status: false,
            taskId: response.body.id,
          },
        ],
      });
    });

    it('should return 401', async () => {
      await supertest(app.getHttpServer())
        .post('/task')
        .expect(HttpStatus.UNAUTHORIZED);
    });
  });

  describe('/task/:id PATCH', () => {
    it('should change task status', async () => {
      const user = await userFactory.create();
      const task = await taskFactory.create({ userId: user.id });
      const token = login(user);

      const response = await supertest(app.getHttpServer())
        .patch(`/task/${task.id}/status`)
        .send({
          status: true,
        })
        .set('Authorization', `Bearer ${token}`)
        .expect(HttpStatus.OK);

      expect(response.body).toMatchObject({
        status: true,
      });
    });

    it('should return 400 (task not exists)', async () => {
      const user = await userFactory.create();
      const token = login(user);
      const id = faker.datatype.uuid();

      const response = await supertest(app.getHttpServer())
        .patch(`/task/${id}/status`)
        .send({
          status: true,
        })
        .set('Authorization', `Bearer ${token}`)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(response.body.message[0].errors[0]).toBe(
        `data ${id} is not exists`,
      );
    });
  });

  describe('/task GET', () => {
    it('should return all tasks', async () => {
      const user = await userFactory.create();
      await taskFactory.createMany(3, {
        userId: user.id,
        checklists: [{ title: 'judul1' }, { title: 'judul2' }],
      });

      const token = login(user);

      const response = await supertest(app.getHttpServer())
        .get('/task')
        .set('Authorization', `Bearer ${token}`);

      expect(response.body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.any(String),
            title: expect.any(String),
            description: expect.any(String),
            userId: user.id,
          }),
        ]),
      );
    });
  });

  describe('/task:id PUT', () => {
    it('should return 403 forbiden error', async () => {
      const user = await userFactory.create({
        email: 'test@mail.com',
      });
      const token = login(user);
      const secondUser = await userFactory.create();
      const task = await taskFactory.create({ userId: secondUser.id });

      const response = await supertest(app.getHttpServer())
        .put(`/task/${task.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          title: 'judul',
          description: 'deskripsi',
        })
        .expect(HttpStatus.FORBIDDEN);

      expect(response.body.message).toBe('this task is not belongs to you');
    });

    it('should return 422 error (id not exists)', async () => {
      const user = await userFactory.create();
      const token = login(user);
      const id = faker.datatype.uuid();

      const response = await supertest(app.getHttpServer())
        .put(`/task/${id}`)
        .send({
          title: 'judul',
          description: 'deskripsi',
        })
        .set('Authorization', `Bearer ${token}`)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(response.body.message[0].errors[0]).toBe(
        `data ${id} is not exists`,
      );
    });

    it('should return updated task', async () => {
      const user = await userFactory.create();
      const task = await taskFactory.create({ userId: user.id });
      const token = login(user);

      const response = await supertest(app.getHttpServer())
        .put(`/task/${task.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          title: 'judul',
          description: 'deskripsi',
        })
        .expect(HttpStatus.OK);

      expect(response.body).toMatchObject({
        id: task.id,
        title: 'judul',
        description: 'deskripsi',
        userId: user.id,
      });
    });
  });

  describe('/task/:id DELETE', () => {
    it('should delete task', async () => {
      const user = await userFactory.create();
      const task = await taskFactory.create({
        userId: user.id,
      });

      const token = login(user);

      const response = await supertest(app.getHttpServer())
        .delete(`/task/${task.id}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(HttpStatus.OK);

      expect(response.body).toMatchObject({
        status: true,
      });
    });

    it('should return 403', async () => {
      const user = await userFactory.create();
      const secondUser = await userFactory.create({
        email: 'mail@mail.com',
      });
      const task = await taskFactory.create({
        userId: secondUser.id,
      });

      const token = login(user);

      const response = await supertest(app.getHttpServer())
        .delete(`/task/${task.id}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(HttpStatus.FORBIDDEN);

      expect(response.body.message).toBe('this task is not belongs to you');
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
