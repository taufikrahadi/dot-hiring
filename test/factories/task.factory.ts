import { Factory } from '@linnify/typeorm-factory';
import { Task } from '../../src/business/task/task.entity';
import * as faker from 'faker';

export class TaskFactory extends Factory<Task> {
  entity = Task;

  title = faker.name.title();
  description = faker.lorem.sentence();
  userId = '';
}
