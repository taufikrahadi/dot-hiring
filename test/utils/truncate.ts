import { getConnection, IsNull, Not } from 'typeorm';

export const truncate = async () => {
  const entities = ['task_checklist', 'task', 'user'];
  for (const entity of entities) {
    const repository = getConnection().getRepository(entity);
    await repository.delete({
      id: Not(IsNull()),
    });
  }
};
