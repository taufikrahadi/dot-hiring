FROM node:17-buster-slim

WORKDIR /app

COPY package.json .

RUN yarn install

COPY . .

RUN yarn global add rimraf

RUN yarn build

EXPOSE 8080

CMD [ "yarn", "start" ]
