import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../common/guards/auth.guard';
import { CreateTaskDto } from '../../../business/task/dto/create-task.dto';
import { TaskService } from '../../../business/task/task.service';
import { UserInfo } from '../../common/decorators/userinfo.decorator';
import { WithIdDto } from '../../../business/task/dto/with-id.dto';
import { UpdateTaskDto } from '../../../business/task/dto/update-task.dto';
import { ApiOkResponse, ApiSecurity } from '@nestjs/swagger';
import { Task } from '../../../business/task/task.entity';

@Controller('task')
@ApiSecurity('jwt')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: Task, status: HttpStatus.CREATED })
  async createTask(
    @Body() createTaskDto: CreateTaskDto,
    @UserInfo('id') userId: string,
  ) {
    return this.taskService.createTask({
      ...createTaskDto,
      userId,
    });
  }

  @Patch('/:id/status')
  @UseGuards(JwtAuthGuard)
  async updateStatus(
    @Param() { id }: WithIdDto,
    @Body() status: boolean,
    @UserInfo('id') userId: string,
  ) {
    await this.taskService.checkTaskUserId(id, userId);
    return this.taskService.updateTaskStatus(id, status);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: [Task] })
  async getAllMyTasks(@UserInfo('id') id: string) {
    return this.taskService.findByUserId(id);
  }

  @Put('/:id')
  @UseGuards(JwtAuthGuard)
  async updateTask(
    @Param() { id }: WithIdDto,
    @Body() updateTaskDto: UpdateTaskDto,
    @UserInfo('id') userId: string,
  ) {
    await this.taskService.checkTaskUserId(id, userId);
    return this.taskService.updateTask(id, updateTaskDto);
  }

  @Delete('/:id')
  @UseGuards(JwtAuthGuard)
  async deleteTask(@Param() { id }: WithIdDto, @UserInfo('id') userId: string) {
    await this.taskService.checkTaskUserId(id, userId);
    return this.taskService.deleteTask(id);
  }
}
