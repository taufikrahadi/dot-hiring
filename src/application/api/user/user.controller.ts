import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../common/guards/auth.guard';
import { CreateUserDto } from '../../../business/user/dto/create.dto';
import { UserService } from '../../../business/user/user.service';
import { UserInfo } from '../../common/decorators/userinfo.decorator';
import { ApiOkResponse, ApiSecurity } from '@nestjs/swagger';
import { User } from '../../../business/user/user.entity';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('register')
  @ApiOkResponse({ type: User, status: HttpStatus.CREATED })
  registerUser(@Body() createUserDto: CreateUserDto) {
    return this.userService.createUser(createUserDto);
  }

  @Get('profile')
  @ApiSecurity('jwt')
  @ApiOkResponse({ type: User, status: HttpStatus.OK })
  @UseGuards(JwtAuthGuard)
  userProfile(@UserInfo('id') userId: string) {
    return this.userService.findById(userId);
  }
}
