import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './api/auth/auth.module';
import { TaskModule } from './api/task/task.module';
import { UserModule } from './api/user/user.module';
import { DatabaseConfig } from './configs/database.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    DatabaseConfig,

    // API Modules
    UserModule,
    AuthModule,
    TaskModule,
  ],
})
export class AppModule {}
