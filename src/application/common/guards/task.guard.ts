import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { WithIdDto } from 'src/business/task/dto/with-id.dto';
import { TaskService } from '../../../business/task/task.service';

@Injectable()
export class TaskGuard implements CanActivate {
  constructor(private readonly taskService: TaskService) {}

  async canActivate(context: ExecutionContext) {
    try {
      const req = context.switchToHttp().getRequest();
      const userId = req.user.id;
      const { id }: WithIdDto = req.params;

      const task = await this.taskService.findById(id);
      if (!task)
        throw new ForbiddenException(`this task is not belongs to you`);

      // if (task.userId !== userId)
      //   throw new UnprocessableEntityException('Task not found');

      return true;
    } catch (error) {
      throw error;
    }
  }
}
