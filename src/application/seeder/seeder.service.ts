import { Injectable, Logger } from '@nestjs/common';
import { UserService } from '../../business/user/user.service';
import { customAlphabet } from 'nanoid';

@Injectable()
export class SeederService {
  constructor(
    private readonly userService: UserService,
    private readonly logger: Logger,
  ) {}

  async seed() {
    return Promise.all([
      this.createUser()
        .then((user) => {
          this.logger.debug('Successfully created user');
          Promise.resolve(user);
        })
        .catch((err) => {
          this.logger.debug('Failed to create user');
          Promise.reject(err);
        }),
    ]);
  }

  async createUser() {
    try {
      const findUser = await this.userService.findByEmailWithPassword(
        'test@mail.com',
      );
      const usePassword = 'password';

      if (findUser) await this.userService.deleteUser(findUser.id);

      this.logger.debug(`user test@mail.com password: ${usePassword}`);
      return await this.userService.createUser({
        email: 'test@mail.com',
        password: usePassword,
        fullname: 'test user',
      });
    } catch (error) {
      throw error;
    }
  }
}
