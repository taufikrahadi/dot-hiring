import { Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskChecklist } from '../../business/task-checklist/task-checklist.entity';
import { Task } from '../../business/task/task.entity';
import { User } from '../../business/user/user.entity';
import { UserModule } from '../api/user/user.module';
import { SeederService } from './seeder.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DB_HOST'),
        port: configService.get('DB_PORT'),
        username: configService.get('DB_USER'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME'),
        entities: [User, Task, TaskChecklist],
        synchronize: true,
        logging: ['query'],
      }),
      inject: [ConfigService],
    }),

    UserModule,
  ],
  providers: [SeederService, Logger],
})
export class SeederModule {}
