import {
  Logger,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { ValidationError } from 'class-validator';
import { AppModule } from './application/app.module';
import * as morgan from 'morgan';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get<ConfigService>(ConfigService);
  const mainApplicationLog = new Logger('MainApplication');
  const incomingRequestLog = new Logger('IncomingRequest');

  const port = configService.get<number>('PORT') ?? 8080;

  app.setGlobalPrefix('/api');

  const swaggerConfig = new DocumentBuilder()
    .setTitle('NestJS API')
    .setDescription('API for simple todo application with authentication')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document);

  app.enableCors({
    origin: '*',
  });

  /**
   * produces errors messages in json format with status code 422
   *
   */
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: ValidationError[]) => {
        const message = errors.map((error) => {
          const { property, constraints } = error;
          const keys = Object.keys(constraints);

          const msgs: string[] = [];

          keys.forEach((key) => {
            msgs.push(`${constraints[key]}`);
          });

          return {
            property,
            errors: msgs,
          };
        });

        throw new UnprocessableEntityException(message);
      },
    }),
  );

  app.use(
    morgan('common', {
      stream: {
        write: (message) => incomingRequestLog.verbose(message),
      },
    }),
  );

  await app.listen(port);
  mainApplicationLog.log(`Application listening on port ${port}`);
}
bootstrap();
