import { BaseEntity } from '../../resources/class/BaseEntity';
import { BeforeInsert, Column, Entity, OneToMany } from 'typeorm';
import { genSaltSync, hash } from 'bcrypt';
import { Task } from '../task/task.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class User extends BaseEntity {
  @Column({
    unique: true,
  })
  @ApiProperty()
  email: string;

  @Column()
  @ApiProperty()
  fullname: string;

  @Column({
    select: false,
  })
  password: string;

  @OneToMany(() => Task, (task) => task.user, {})
  task: Task[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await hash(this.password, genSaltSync(12));
  }
}
