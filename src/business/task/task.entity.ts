import { BaseEntity } from '../../resources/class/BaseEntity';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { User } from '../user/user.entity';
import { TaskChecklist } from '../task-checklist/task-checklist.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Task extends BaseEntity {
  @Column()
  @ApiProperty()
  title: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  @ApiProperty()
  description?: string;

  @Column({
    type: 'uuid',
  })
  @ApiProperty()
  userId: string;

  @Column({
    default: false,
  })
  @ApiProperty({ type: Boolean })
  status?: boolean;

  @ManyToOne(() => User, {
    cascade: ['remove'],
  })
  user?: User;

  @OneToMany(() => TaskChecklist, (taskChecklist) => taskChecklist.task, {
    cascade: ['insert', 'remove', 'update'],
    eager: true,
  })
  @ApiProperty({ type: [TaskChecklist] })
  checklists?: TaskChecklist[];
}
