import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty } from 'class-validator';

export class UpdateStatusDto {
  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty()
  status: boolean;
}
