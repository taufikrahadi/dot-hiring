import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateTaskChecklistDto } from '../../task-checklist/dto/create-task-checklist.dto';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTaskDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  title: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  description: string;

  @IsArray()
  @ApiProperty({ type: [CreateTaskChecklistDto] })
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => CreateTaskChecklistDto)
  checklists: CreateTaskChecklistDto[];
}
