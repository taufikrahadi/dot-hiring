import { ApiProperty } from '@nestjs/swagger';
import { IsExists } from '../../../resources/utils/is-exists.validator';
import { Task } from '../task.entity';

export class WithIdDto {
  @IsExists('id', Task)
  @ApiProperty()
  id: string;
}
