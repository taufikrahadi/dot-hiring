import {
  ForbiddenException,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './task.entity';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task) private readonly taskRepository: Repository<Task>,
  ) {}

  async createTask(createTaskDto: Task) {
    try {
      const task = await this.taskRepository.save(createTaskDto);

      return task;
    } catch (error) {
      throw error;
    }
  }

  async findById(id: string) {
    try {
      const task = await this.taskRepository.findOne({
        where: {
          id,
        },
      });

      return task;
    } catch (error) {
      throw error;
    }
  }

  async findByUserId(userId: string) {
    try {
      const tasks = await this.taskRepository.find({
        userId,
      });

      return tasks;
    } catch (error) {
      throw error;
    }
  }

  async updateTask(taskId: string, updateTaskDto: UpdateTaskDto) {
    try {
      await this.taskRepository.update(taskId, {
        title: updateTaskDto.title,
        description: updateTaskDto.description,
        // status: updateTaskDto.status,
        // checklists: updateTaskDto.checklists,
      });

      return await this.findById(taskId);
    } catch (error) {
      throw error;
    }
  }

  async updateTaskStatus(taskId: string, status: boolean) {
    try {
      await this.taskRepository.update(
        {
          id: taskId,
        },
        {
          status,
        },
      );

      return status;
    } catch (error) {
      throw error;
    }
  }

  async checkTaskUserId(taskId: string, userId: string) {
    try {
      const task = await this.findById(taskId);
      if (task.userId !== userId)
        throw new ForbiddenException(`this task is not belongs to you`);

      return true;
    } catch (error) {
      throw error;
    }
  }

  async deleteTask(taskId: string) {
    try {
      await this.taskRepository.delete({
        id: taskId,
      });

      return { status: true };
    } catch (error) {
      throw error;
    }
  }
}
