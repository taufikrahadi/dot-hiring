export interface Token {
  id: string;
  email: string;
  fullname: string;
}
