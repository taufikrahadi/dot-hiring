import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { User } from '../../../business/user/user.entity';
import { IsExists } from '../../../resources/utils/is-exists.validator';

export class LoginDto {
  @IsEmail()
  @IsExists('email', User)
  @IsNotEmpty()
  @ApiProperty()
  email: string;

  @IsString()
  @ApiProperty()
  @IsNotEmpty()
  password: string;
}

export class LoginResponse {
  @ApiProperty()
  userId: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  accessToken: string;
}
