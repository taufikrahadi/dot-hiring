import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';
import { UserService } from '../user/user.service';
import { LoginDto } from './dto/login.dto';
import { Token } from './interfaces/token.interface';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async login(loginDto: LoginDto) {
    try {
      const user = await this.userService.findByEmailWithPassword(
        loginDto.email,
      );

      this.comparePassword(loginDto.password, user.password);

      const accessToken = this.signToken({
        id: user.id,
        email: user.email,
        fullname: user.fullname,
      });

      return {
        userId: user.id,
        email: user.email,
        accessToken,
      };
    } catch (error) {
      throw error;
    }
  }

  private comparePassword(password: string, hash: string) {
    const compare = compareSync(password, hash);

    if (!compare) throw new BadRequestException('Wrong Password');
  }

  private signToken(payload: Token) {
    const token = this.jwtService.sign(payload);

    return token;
  }
}
