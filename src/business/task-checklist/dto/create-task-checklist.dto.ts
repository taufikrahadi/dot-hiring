import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTaskChecklistDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  title: string;
}
