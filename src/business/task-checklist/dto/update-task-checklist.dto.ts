import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, IsUUID } from 'class-validator';

export class UpdateTaskChecklistDto {
  @IsUUID()
  @IsOptional()
  @ApiProperty()
  id: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  title: string;

  @IsBoolean()
  @ApiProperty()
  @IsOptional()
  status: boolean;
}
