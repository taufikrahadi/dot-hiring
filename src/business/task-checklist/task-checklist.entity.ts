import { BaseEntity } from '../../resources/class/BaseEntity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { Task } from '../task/task.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class TaskChecklist extends BaseEntity {
  @Column({
    type: 'uuid',
  })
  @ApiProperty()
  taskId?: string;

  @ApiProperty()
  @Column()
  title: string;

  @ApiProperty({ type: Boolean })
  @Column({
    default: false,
  })
  status?: boolean;

  @ManyToOne(() => Task, {
    onDelete: 'CASCADE',
  })
  task?: Task;
}
