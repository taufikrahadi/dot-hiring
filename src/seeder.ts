import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SeederModule } from './application/seeder/seeder.module';
import { SeederService } from './application/seeder/seeder.service';

async function bootstrap() {
  NestFactory.createApplicationContext(SeederModule)
    .then((appContext) => {
      const logger = appContext.get<Logger>(Logger);
      const seeder = appContext.get<SeederService>(SeederService);

      seeder
        .seed()
        .then(() => {
          logger.log('seeding complete');
        })
        .catch((err) => {
          logger.error(err);
        })
        .finally(() => {
          appContext.close();
        });
    })
    .catch((err) => {
      throw err;
    });
}

bootstrap();
