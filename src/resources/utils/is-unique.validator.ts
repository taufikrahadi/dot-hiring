import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { getRepository } from 'typeorm';

export function IsUnique(
  key: string,
  repository: any,
  validationOptions?: ValidationOptions,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: {
        message: ({ value }) => `${value} already in use`,
        ...validationOptions,
      },
      constraints: [key, repository],
      validator: IsUniqueConstraints,
    });
  };
}

@ValidatorConstraint({ name: 'IsUnique' })
export class IsUniqueConstraints implements ValidatorConstraintInterface {
  async validate(value: any, args: ValidationArguments) {
    const [key, repo] = args.constraints;
    const isUnique = await getRepository(repo)
      .createQueryBuilder('alias')
      .where(`alias.${key} = :id`, { id: value })
      .getOne();

    return !Boolean(isUnique);
  }
}
